package wicket.template;
//今何を表示しているのかわかりやすくしたいby山下さん
//復元したさいは保存ボタンの表示をなくしたいby山下さん
//APサーバとWebサーバ
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.list.PageableListView;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigator;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.seasar.doma.Dao;
import org.seasar.doma.jdbc.tx.TransactionManager;

import entity.Pattern;
import entity.User;

public class SaveItemPanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private PatternDao dao;
	private Optional<Pattern> pattern;
	private User user;

	private List<String> patternNames;

	public SaveItemPanel(String id,User user) {
		super(id);
		this.user=user;
	}

	@Override
	protected void onInitialize() {
		// TODO Auto-generated method stub
		super.onInitialize();

		try {

			TransactionManager tm1 = DBConfig.singleton()
					.getTransactionManager();

			tm1.required(() -> {

				patternNames = dao.selectItem(user.userName);

			});

			PageableListView<String> saveItems = new PageableListView<String>(
					"paternNames", patternNames, 5) {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				protected void populateItem(ListItem<String> item) {
					final String saveNameString = item.getModelObject();

					Link<String> link = new Link<String>("restoration") {
						/**
						 * 
						 */
						private static final long serialVersionUID = 1L;

						@Override
						public void onClick() {
							try {

								TransactionManager tm1 = DBConfig.singleton()
										.getTransactionManager();

								tm1.required(() -> {
									pattern = dao.select(saveNameString);
								});
								
								SearchPage searchPage = new SearchPage(pattern,user);
								setResponsePage(searchPage);
								//Label label = new Label("saveNameString", "<b>"+saveNameString+"</b>");
								//label.setEscapeModelStrings(false);
								//add(label);
								
							} catch (Exception e) {

							}
						}
					};
					item.add(link);
					Label label = new Label("saveNameString",saveNameString);
					link.add(label);
				}
			};

			add(saveItems);
			add(new PagingNavigator("navigator", saveItems));

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@Override
	protected void onConfigure() {
		super.onConfigure();
		setOutputMarkupPlaceholderTag(true);
	}
	
	public Optional<Pattern> getPattern(){
		return pattern;
	}

}
