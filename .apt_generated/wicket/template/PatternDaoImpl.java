package wicket.template;

/** */
@javax.annotation.Generated(value = { "Doma", "2.2.0" }, date = "2015-08-10T19:28:06.853+0900")
public class PatternDaoImpl extends org.seasar.doma.internal.jdbc.dao.AbstractDao implements wicket.template.PatternDao {

    static {
        org.seasar.doma.internal.Artifact.validateVersion("2.2.0");
    }

    private static final java.lang.reflect.Method __method0 = org.seasar.doma.internal.jdbc.dao.AbstractDao.getDeclaredMethod(wicket.template.PatternDao.class, "insert", entity.Pattern.class);

    private static final java.lang.reflect.Method __method1 = org.seasar.doma.internal.jdbc.dao.AbstractDao.getDeclaredMethod(wicket.template.PatternDao.class, "select", java.lang.String.class);

    private static final java.lang.reflect.Method __method2 = org.seasar.doma.internal.jdbc.dao.AbstractDao.getDeclaredMethod(wicket.template.PatternDao.class, "selectItem", java.lang.String.class);

    private static final java.lang.reflect.Method __method3 = org.seasar.doma.internal.jdbc.dao.AbstractDao.getDeclaredMethod(wicket.template.PatternDao.class, "update", entity.Pattern.class);

    /** */
    public PatternDaoImpl() {
        super(wicket.template.DBConfig.singleton());
    }

    /**
     * @param connection the connection
     */
    public PatternDaoImpl(java.sql.Connection connection) {
        super(wicket.template.DBConfig.singleton(), connection);
    }

    /**
     * @param dataSource the dataSource
     */
    public PatternDaoImpl(javax.sql.DataSource dataSource) {
        super(wicket.template.DBConfig.singleton(), dataSource);
    }

    /**
     * @param config the configuration
     */
    protected PatternDaoImpl(org.seasar.doma.jdbc.Config config) {
        super(config);
    }

    /**
     * @param config the configuration
     * @param connection the connection
     */
    protected PatternDaoImpl(org.seasar.doma.jdbc.Config config, java.sql.Connection connection) {
        super(config, connection);
    }

    /**
     * @param config the configuration
     * @param dataSource the dataSource
     */
    protected PatternDaoImpl(org.seasar.doma.jdbc.Config config, javax.sql.DataSource dataSource) {
        super(config, dataSource);
    }

    @Override
    public int insert(entity.Pattern pattern) {
        entering("wicket.template.PatternDaoImpl", "insert", pattern);
        try {
            if (pattern == null) {
                throw new org.seasar.doma.DomaNullPointerException("pattern");
            }
            org.seasar.doma.jdbc.query.AutoInsertQuery<entity.Pattern> __query = getQueryImplementors().createAutoInsertQuery(__method0, entity._Pattern.getSingletonInternal());
            __query.setMethod(__method0);
            __query.setConfig(__config);
            __query.setEntity(pattern);
            __query.setCallerClassName("wicket.template.PatternDaoImpl");
            __query.setCallerMethodName("insert");
            __query.setQueryTimeout(-1);
            __query.setSqlLogType(org.seasar.doma.jdbc.SqlLogType.FORMATTED);
            __query.setNullExcluded(false);
            __query.setIncludedPropertyNames();
            __query.setExcludedPropertyNames();
            __query.prepare();
            org.seasar.doma.jdbc.command.InsertCommand __command = getCommandImplementors().createInsertCommand(__method0, __query);
            int __result = __command.execute();
            __query.complete();
            exiting("wicket.template.PatternDaoImpl", "insert", __result);
            return __result;
        } catch (java.lang.RuntimeException __e) {
            throwing("wicket.template.PatternDaoImpl", "insert", __e);
            throw __e;
        }
    }

    @Override
    public java.util.Optional<entity.Pattern> select(java.lang.String patternName) {
        entering("wicket.template.PatternDaoImpl", "select", patternName);
        try {
            org.seasar.doma.jdbc.query.SqlFileSelectQuery __query = getQueryImplementors().createSqlFileSelectQuery(__method1);
            __query.setMethod(__method1);
            __query.setConfig(__config);
            __query.setSqlFilePath("META-INF/wicket/template/PatternDao/select.sql");
            __query.setEntityType(entity._Pattern.getSingletonInternal());
            __query.addParameter("patternName", java.lang.String.class, patternName);
            __query.setCallerClassName("wicket.template.PatternDaoImpl");
            __query.setCallerMethodName("select");
            __query.setResultEnsured(false);
            __query.setResultMappingEnsured(false);
            __query.setFetchType(org.seasar.doma.FetchType.LAZY);
            __query.setQueryTimeout(-1);
            __query.setMaxRows(-1);
            __query.setFetchSize(-1);
            __query.setSqlLogType(org.seasar.doma.jdbc.SqlLogType.FORMATTED);
            __query.prepare();
            org.seasar.doma.jdbc.command.SelectCommand<java.util.Optional<entity.Pattern>> __command = getCommandImplementors().createSelectCommand(__method1, __query, new org.seasar.doma.internal.jdbc.command.OptionalEntitySingleResultHandler<entity.Pattern>(entity._Pattern.getSingletonInternal()));
            java.util.Optional<entity.Pattern> __result = __command.execute();
            __query.complete();
            exiting("wicket.template.PatternDaoImpl", "select", __result);
            return __result;
        } catch (java.lang.RuntimeException __e) {
            throwing("wicket.template.PatternDaoImpl", "select", __e);
            throw __e;
        }
    }

    @Override
    public java.util.List<java.lang.String> selectItem(java.lang.String userName) {
        entering("wicket.template.PatternDaoImpl", "selectItem", userName);
        try {
            org.seasar.doma.jdbc.query.SqlFileSelectQuery __query = getQueryImplementors().createSqlFileSelectQuery(__method2);
            __query.setMethod(__method2);
            __query.setConfig(__config);
            __query.setSqlFilePath("META-INF/wicket/template/PatternDao/selectItem.sql");
            __query.addParameter("userName", java.lang.String.class, userName);
            __query.setCallerClassName("wicket.template.PatternDaoImpl");
            __query.setCallerMethodName("selectItem");
            __query.setResultEnsured(false);
            __query.setResultMappingEnsured(false);
            __query.setFetchType(org.seasar.doma.FetchType.LAZY);
            __query.setQueryTimeout(-1);
            __query.setMaxRows(-1);
            __query.setFetchSize(-1);
            __query.setSqlLogType(org.seasar.doma.jdbc.SqlLogType.FORMATTED);
            __query.prepare();
            org.seasar.doma.jdbc.command.SelectCommand<java.util.List<java.lang.String>> __command = getCommandImplementors().createSelectCommand(__method2, __query, new org.seasar.doma.internal.jdbc.command.BasicResultListHandler<java.lang.String>(org.seasar.doma.wrapper.StringWrapper::new));
            java.util.List<java.lang.String> __result = __command.execute();
            __query.complete();
            exiting("wicket.template.PatternDaoImpl", "selectItem", __result);
            return __result;
        } catch (java.lang.RuntimeException __e) {
            throwing("wicket.template.PatternDaoImpl", "selectItem", __e);
            throw __e;
        }
    }

    @Override
    public int update(entity.Pattern pattern) {
        entering("wicket.template.PatternDaoImpl", "update", pattern);
        try {
            if (pattern == null) {
                throw new org.seasar.doma.DomaNullPointerException("pattern");
            }
            org.seasar.doma.jdbc.query.AutoUpdateQuery<entity.Pattern> __query = getQueryImplementors().createAutoUpdateQuery(__method3, entity._Pattern.getSingletonInternal());
            __query.setMethod(__method3);
            __query.setConfig(__config);
            __query.setEntity(pattern);
            __query.setCallerClassName("wicket.template.PatternDaoImpl");
            __query.setCallerMethodName("update");
            __query.setQueryTimeout(-1);
            __query.setSqlLogType(org.seasar.doma.jdbc.SqlLogType.FORMATTED);
            __query.setNullExcluded(false);
            __query.setVersionIgnored(false);
            __query.setIncludedPropertyNames();
            __query.setExcludedPropertyNames();
            __query.setUnchangedPropertyIncluded(false);
            __query.setOptimisticLockExceptionSuppressed(false);
            __query.prepare();
            org.seasar.doma.jdbc.command.UpdateCommand __command = getCommandImplementors().createUpdateCommand(__method3, __query);
            int __result = __command.execute();
            __query.complete();
            exiting("wicket.template.PatternDaoImpl", "update", __result);
            return __result;
        } catch (java.lang.RuntimeException __e) {
            throwing("wicket.template.PatternDaoImpl", "update", __e);
            throw __e;
        }
    }

}
