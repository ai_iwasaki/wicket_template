package entity;

import java.io.Serializable;

import org.seasar.doma.Entity;
import org.seasar.doma.Id;
import org.seasar.doma.jdbc.entity.NamingType;

@Entity(naming = NamingType.SNAKE_LOWER_CASE)
public class Pattern implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id public int patternId;
	public int primerLength;
	public double tmValue;
	public int targetSequenceStart;
	public int targetSequenceEnd;
	public int region;
	public String patternName;
	public String userName;
	
}
