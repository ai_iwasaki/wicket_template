package wicket.template;

import java.util.Optional;

import org.seasar.doma.Dao;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

import entity.User;


@Dao(config = DBConfig.class)
public interface UserDao {
	@Select
    public Optional<User> select(String userName);
	
	@Insert
	public int insert(User user);
	
	@Update
	public int update(User user);
	
}
