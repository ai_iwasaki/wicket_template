package wicket.template;
//リザルトページの位置も指定する必要がある
import java.awt.print.Printable;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.wicket.Page;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.omg.CORBA.PRIVATE_MEMBER;
import org.seasar.doma.jdbc.UniqueConstraintException;
import org.seasar.doma.jdbc.tx.TransactionManager;

import com.google.inject.Inject;

import entity.Pattern;
import entity.User;

public class SearchPage extends WebPage {
	private static final long serialVersionUID = 1L;

	@Inject
	private PatternDao patternDao;
	private Pattern pattern=new Pattern();
	private UserDao userDao;
	private User user=new User();
	private String out3Primer;
	private String out5Primer;
	private ModalWindow modalWindow;

	public SearchPage(final PageParameters parameters) {
		super(parameters);
	}

	public SearchPage(Optional<Pattern> pattern,User user) {
		this.pattern=pattern.get();
		this.user=user;
	}
	
	public SearchPage(User user){
		this.user=user;
	}
	
	public SearchPage() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void onInitialize() {
		super.onInitialize();
		// TransactionManager tm1 =
		// DBConfig.singleton().getTransactionManager();
		//
		// tm1.required(() -> {
		// Pattern pattern1 = dao.select(28).get();//28の中身がpattern1には入ってる。
		// System.out.println(pattern1);
		// });

		if(user.userName==null){
			setResponsePage(new LoginPage());
		}
		
		List<String> list = new ArrayList<String>();
		list.add("Ciona");

		Form<Void> form = new Form<>("form");
		add(form);

		DropDownChoice<String> drop1 = new DropDownChoice<String>("species",
				new Model<String>(), list);
		form.add(drop1);

		TextField<Integer> text1 = new TextField<>("primerLen",
				new PropertyModel<Integer>(pattern, "primerLength"));
		form.add(text1);

		TextField<Double> text2 = new TextField<>("tmVal",
				new PropertyModel<Double>(pattern, "tmValue"));
		form.add(text2);

		TextField<Integer> text3 = new TextField<>("targetStart",
				new PropertyModel<Integer>(pattern, "targetSequenceStart"));
		form.add(text3);

		TextField<Integer> text4 = new TextField<Integer>("targetEnd",
				new PropertyModel<Integer>(pattern, "targetSequenceEnd")) {
			
		};
		form.add(text4);

		TextField<Integer> text5 = new TextField<>("primerRegion",
				new PropertyModel<Integer>(pattern, "region"));
		form.add(text5);
		ResultPage resultPage = new ResultPage("result",
				new PropertyModel<Pattern>(this, "pattern"),
				new PropertyModel<String>(this, "out3Primer"),
				new PropertyModel<String>(this, "out5Primer"));
		add(resultPage);
		resultPage.setVisible(false);
		
		SaveItemPanel saveItemPanel = new SaveItemPanel("saveItemPanel",user);
		
		add(saveItemPanel);
		saveItemPanel.setVisible(true);
		
		FeedbackPanel feedbackPanel = new FeedbackPanel("feedback") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void onConfigure() {
			//protected void onRender() {
				// TODO Auto-generated method stub
				super.onConfigure();
//				super.onRender();
				setOutputMarkupId(true);
			}
		};

		modalWindow = new ModalWindow("modal");
	    add(modalWindow);
	 
	    modalWindow.setPageCreator(new ModalWindow.PageCreator(){
	      @Override
	      public Page createPage() {
	        return new SavePage(pattern, user, modalWindow);
	      }
	    });
	 
	    modalWindow.setWindowClosedCallback(new ModalWindow.WindowClosedCallback() {
	      @Override
	      public void onClose(AjaxRequestTarget target) {
	    	SearchPage searchPage = new SearchPage(user);
	        setResponsePage(searchPage);
	      }
	    });
		
		Button b1 = new AjaxButton("submitInfo") {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			//
			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				super.onSubmit(target, form);
				System.out.println("きてるうう?");
				// setResponsePage(HomePage.class);UniqueConstraintException
				try {
					TransactionManager tm = DBConfig.singleton()
							.getTransactionManager();

					ProcessBuilder pbBuilder = new ProcessBuilder(
							"/Users/iwasakiai/test.py");
					try {
						Process process = pbBuilder.start();
						InputStream in = process.getInputStream();
						StringBuffer out = new StringBuffer();
						BufferedReader br = new BufferedReader(
								new InputStreamReader(in));
						String line;
						List<String> outList = new ArrayList<String>();
						while ((line = br.readLine()) != null) {
							out.append(line + System.lineSeparator());
							outList.add(line.toString());
						}
						List<String> out3PrimerList = new ArrayList<String>();
						List<String> out5PrimerList = new ArrayList<String>();
						out3PrimerList.add(outList.get(0));
						out5PrimerList.add(outList.get(1));
						out3Primer = out3PrimerList.toString();
						out5Primer = out5PrimerList.toString();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						System.out.println("きてる?");
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println("きてる？？");
					}
					System.out.println("きてる？？？？");
					resultPage.setVisible(true);
					target.add(resultPage);
				} catch (UniqueConstraintException e) {
					// feedbackPanel.info(message);
					info("既に登録されています");
					target.add(feedbackPanel);
				}

			}
			

			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
				// TODO Auto-generated method stub
				super.onError(target, form);
				System.out.println("ここ？");
				target.add(feedbackPanel);
				//setResponsePage(getPage());
			}
			
		};

		Button b2 = new AjaxButton("save") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			//
			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				super.onSubmit(target, form);
				// setResponsePage(HomePage.class);UniqueConstraintException
				try {
					modalWindow.show(target);
					
				} catch (UniqueConstraintException e) {
					// feedbackPanel.info(message);
					info("既に登録されています");
					target.add(feedbackPanel);
				}

			}
		};

		form.add(b1);
		form.add(b2);
		form.add(feedbackPanel);

	}
}
