package wicket.template;

import javax.inject.Inject;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.seasar.doma.jdbc.UniqueConstraintException;
import org.seasar.doma.jdbc.tx.TransactionManager;

import entity.User;

public class CreateIdPage extends WebPage {

	@Inject
	private UserDao dao;
	private User user;

	PageParameters parameters;

	public CreateIdPage() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onInitialize() {
		// TODO Auto-generated method stub
		super.onInitialize();
		user = new User();

		Form<Void> form = new Form<>("form");
		add(form);

		TextField<String> text1 = new TextField<>("userName",
				new PropertyModel<String>(user, "userName"));
		form.add(text1);

		TextField<String> text2 = new TextField<>("password",
				new PropertyModel<String>(user, "password"));
		form.add(text2);

		Model<String> conformPassModel = new Model<String>();
		
		TextField<String> text3 = new TextField<>("conformPassword",conformPassModel);
		form.add(text3);

		FeedbackPanel feedbackPanel = new FeedbackPanel("feedback") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void onConfigure() {
				// TODO Auto-generated method stub
				super.onConfigure();
				setOutputMarkupId(true);
			}
		};

		Button b1 = new AjaxButton("login") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			//
			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				super.onSubmit(target, form);
				try {
					TransactionManager tm = DBConfig.singleton()
							.getTransactionManager();

					tm.required(() -> {
						if(user.password.equals(conformPassModel.getObject().toString())){
							dao.insert(user);
							SearchPage homePage = new SearchPage(parameters);
							setResponsePage(homePage);
						}
						else{
							 info("パスワードと確認用パスワードが一致しません");
							 target.add(feedbackPanel);
						 }
					});
				} catch (UniqueConstraintException e) {
					// feedbackPanel.info(message);
					info("既に登録されています");
					target.add(feedbackPanel);
				} 
					catch (NullPointerException e) {
					info("ID・パスワードを入力してください");
					target.add(feedbackPanel);
				}
				

			};

		};
		form.add(b1);
		form.add(feedbackPanel);
		
		Link<String> link = new Link<String>("loginpage") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				LoginPage loginPage = new LoginPage(parameters);
				setResponsePage(loginPage);
			}
		};

		form.add(link);
	}
}
