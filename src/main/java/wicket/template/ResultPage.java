package wicket.template;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

import entity.Pattern;

public class ResultPage extends Panel{
	
	private IModel<String> out3Primer;
	private IModel<String> out5Primer;
	
	public ResultPage(String id,IModel<Pattern> model) {
		super(id,model);
	}
	
	public ResultPage(String id,IModel<Pattern> model,IModel<String> modelString,IModel<String> modelString2) {
		super(id,model);
		out3Primer=modelString;
		out5Primer=modelString2;
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Override
	protected void onInitialize() {
		super.onInitialize();
		Pattern p=(Pattern)getDefaultModelObject();
		//add(new Label("name", p.primerLength));
		
		
		DetailPanel detailPanel = new DetailPanel("detail");
		add(detailPanel);
		detailPanel.setVisible(false);
		

		AjaxLink<String> link = new AjaxLink<String>("square") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {

				detailPanel.setVisible(true);
				target.add(detailPanel);
				System.out.println("ok");
			}

		};
		add(link);
		
		link.add(new AttributeModifier("style", new AbstractReadOnlyModel<String>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public String getObject() {
				int baseSequenceLength=150000000;
				double tmp =(double)p.targetSequenceStart/(double)baseSequenceLength;
				double tmp2 = tmp*500;
				int leftPosition=(int)tmp2+50;
				return "width: 5px;height: 20px; background: red; position: absolute; z-index: 2; top: 250px; margin-left: "+leftPosition+"px";
			}
		}));
		add(new Label("3primer",out3Primer));
		add(new Label("5primer",out5Primer));
	}
	
	@Override
	protected void onConfigure() {
		super.onConfigure();
		setOutputMarkupPlaceholderTag(true);
	}

}
