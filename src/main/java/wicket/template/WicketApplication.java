package wicket.template;

import org.apache.wicket.guice.GuiceComponentInjector;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;

import com.google.inject.AbstractModule;

/**
 * Application object for your web application.
 * If you want to run this application without deploying, run the Start class.
 *
 * @see wicket.template.Start#main(String[])
 */
public class WicketApplication extends WebApplication {
    /**
     * @see org.apache.wicket.Application#getHomePage()
     */
    @Override
    public Class<? extends WebPage> getHomePage() {
        return LoginPage.class;
    }

    /**
     * @see org.apache.wicket.Application#init()
     */
    @Override
    public void init() {
        super.init();
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        getComponentInstantiationListeners().add(new GuiceComponentInjector(this, new AbstractModule() {

            @Override
            protected void configure() {
                // TODO Auto-generated method stub
                bind(PatternDao.class).toInstance(new PatternDaoImpl());
                
                bind(UserDao.class).toInstance(new UserDaoImpl());
                
            }
        }));
        mountPage("createid", CreateIdPage.class);
        mountPage("search", SearchPage.class);
        mountPage("save", SavePage.class);
        //mountPage("detail", DetailPanel.class);
    }
}
