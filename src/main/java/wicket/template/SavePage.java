package wicket.template;
//タイムアウトの設定が必要ですby羽山さん
//セッション管理が必要ですby羽山さん
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.seasar.doma.jdbc.UniqueConstraintException;
import org.seasar.doma.jdbc.tx.TransactionManager;

import com.google.inject.Inject;

import entity.Pattern;
import entity.User;

public class SavePage extends WebPage {

	@Inject
	private PatternDao patternDao;
	private UserDao userDao;

	public SavePage(Pattern pattern, User user, ModalWindow modalWindow) {
		
		Form<Void> form = new Form<>("form");
		add(form);

		TextField<Integer> text1 = new TextField<>("patternName",
				new PropertyModel<Integer>(pattern, "patternName"));
		form.add(text1);
		
		FeedbackPanel feedbackPanel = new FeedbackPanel("feedback") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void onConfigure() {
				// TODO Auto-generated method stub
				super.onConfigure();
				setOutputMarkupId(true);
			}
		};

		Button b1 = new AjaxButton("save") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			//
			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				super.onSubmit(target, form);
				// setResponsePage(HomePage.class);UniqueConstraintException
				try {
					TransactionManager tm = DBConfig.singleton()
							.getTransactionManager();

					tm.required(() -> {
						pattern.userName=user.userName;
						patternDao.insert(pattern);
						info("保存しました");
						target.add(feedbackPanel);
						setResponsePage(getPage());
					});
				} catch (UniqueConstraintException e) {
					// feedbackPanel.info(message);
					info("既に登録されています");
					target.add(feedbackPanel);
				}

			}
		};
		form.add(b1);
		form.add(feedbackPanel);
		
		Button b2 = new AjaxButton("cancel") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			//
			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				super.onSubmit(target, form);
				// setResponsePage(HomePage.class);UniqueConstraintException
				try {
					modalWindow.close(target);
				} catch (UniqueConstraintException e) {
					// feedbackPanel.info(message);
					info("既に登録されています");
					target.add(feedbackPanel);
				}

			}
		};
		form.add(b2);
	}

}
