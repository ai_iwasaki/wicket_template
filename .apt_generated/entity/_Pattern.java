package entity;

/** */
@javax.annotation.Generated(value = { "Doma", "2.2.0" }, date = "2015-08-10T19:25:21.006+0900")
public final class _Pattern extends org.seasar.doma.jdbc.entity.AbstractEntityType<entity.Pattern> {

    static {
        org.seasar.doma.internal.Artifact.validateVersion("2.2.0");
    }

    private static final _Pattern __singleton = new _Pattern();

    private final org.seasar.doma.jdbc.entity.NamingType __namingType = org.seasar.doma.jdbc.entity.NamingType.SNAKE_LOWER_CASE;

    /** the patternId */
    public final org.seasar.doma.jdbc.entity.AssignedIdPropertyType<java.lang.Object, entity.Pattern, java.lang.Integer, Object> $patternId = new org.seasar.doma.jdbc.entity.AssignedIdPropertyType<>(entity.Pattern.class, java.lang.Integer.class, java.lang.Integer.class, () -> new org.seasar.doma.wrapper.IntegerWrapper(), null, null, "patternId", "", __namingType, false);

    /** the patternName */
    public final org.seasar.doma.jdbc.entity.DefaultPropertyType<java.lang.Object, entity.Pattern, java.lang.String, Object> $patternName = new org.seasar.doma.jdbc.entity.DefaultPropertyType<>(entity.Pattern.class, java.lang.String.class, java.lang.String.class, () -> new org.seasar.doma.wrapper.StringWrapper(), null, null, "patternName", "", __namingType, true, true, false);

    /** the primerLength */
    public final org.seasar.doma.jdbc.entity.DefaultPropertyType<java.lang.Object, entity.Pattern, java.lang.Integer, Object> $primerLength = new org.seasar.doma.jdbc.entity.DefaultPropertyType<>(entity.Pattern.class, java.lang.Integer.class, java.lang.Integer.class, () -> new org.seasar.doma.wrapper.IntegerWrapper(), null, null, "primerLength", "", __namingType, true, true, false);

    /** the region */
    public final org.seasar.doma.jdbc.entity.DefaultPropertyType<java.lang.Object, entity.Pattern, java.lang.Integer, Object> $region = new org.seasar.doma.jdbc.entity.DefaultPropertyType<>(entity.Pattern.class, java.lang.Integer.class, java.lang.Integer.class, () -> new org.seasar.doma.wrapper.IntegerWrapper(), null, null, "region", "", __namingType, true, true, false);

    /** the targetSequenceEnd */
    public final org.seasar.doma.jdbc.entity.DefaultPropertyType<java.lang.Object, entity.Pattern, java.lang.Integer, Object> $targetSequenceEnd = new org.seasar.doma.jdbc.entity.DefaultPropertyType<>(entity.Pattern.class, java.lang.Integer.class, java.lang.Integer.class, () -> new org.seasar.doma.wrapper.IntegerWrapper(), null, null, "targetSequenceEnd", "", __namingType, true, true, false);

    /** the targetSequenceStart */
    public final org.seasar.doma.jdbc.entity.DefaultPropertyType<java.lang.Object, entity.Pattern, java.lang.Integer, Object> $targetSequenceStart = new org.seasar.doma.jdbc.entity.DefaultPropertyType<>(entity.Pattern.class, java.lang.Integer.class, java.lang.Integer.class, () -> new org.seasar.doma.wrapper.IntegerWrapper(), null, null, "targetSequenceStart", "", __namingType, true, true, false);

    /** the tmValue */
    public final org.seasar.doma.jdbc.entity.DefaultPropertyType<java.lang.Object, entity.Pattern, java.lang.Double, Object> $tmValue = new org.seasar.doma.jdbc.entity.DefaultPropertyType<>(entity.Pattern.class, java.lang.Double.class, java.lang.Double.class, () -> new org.seasar.doma.wrapper.DoubleWrapper(), null, null, "tmValue", "", __namingType, true, true, false);

    /** the userName */
    public final org.seasar.doma.jdbc.entity.DefaultPropertyType<java.lang.Object, entity.Pattern, java.lang.String, Object> $userName = new org.seasar.doma.jdbc.entity.DefaultPropertyType<>(entity.Pattern.class, java.lang.String.class, java.lang.String.class, () -> new org.seasar.doma.wrapper.StringWrapper(), null, null, "userName", "", __namingType, true, true, false);

    private final java.util.function.Supplier<org.seasar.doma.jdbc.entity.NullEntityListener<entity.Pattern>> __listenerSupplier;

    private final boolean __immutable;

    private final String __catalogName;

    private final String __schemaName;

    private final String __tableName;

    private final boolean __isQuoteRequired;

    private final String __name;

    private final java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<entity.Pattern, ?>> __idPropertyTypes;

    private final java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<entity.Pattern, ?>> __entityPropertyTypes;

    private final java.util.Map<String, org.seasar.doma.jdbc.entity.EntityPropertyType<entity.Pattern, ?>> __entityPropertyTypeMap;

    private _Pattern() {
        __listenerSupplier = () -> ListenerHolder.listener;
        __immutable = false;
        __name = "Pattern";
        __catalogName = "";
        __schemaName = "";
        __tableName = "";
        __isQuoteRequired = false;
        java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<entity.Pattern, ?>> __idList = new java.util.ArrayList<>();
        java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<entity.Pattern, ?>> __list = new java.util.ArrayList<>(8);
        java.util.Map<String, org.seasar.doma.jdbc.entity.EntityPropertyType<entity.Pattern, ?>> __map = new java.util.HashMap<>(8);
        __idList.add($patternId);
        __list.add($patternId);
        __map.put("patternId", $patternId);
        __list.add($patternName);
        __map.put("patternName", $patternName);
        __list.add($primerLength);
        __map.put("primerLength", $primerLength);
        __list.add($region);
        __map.put("region", $region);
        __list.add($targetSequenceEnd);
        __map.put("targetSequenceEnd", $targetSequenceEnd);
        __list.add($targetSequenceStart);
        __map.put("targetSequenceStart", $targetSequenceStart);
        __list.add($tmValue);
        __map.put("tmValue", $tmValue);
        __list.add($userName);
        __map.put("userName", $userName);
        __idPropertyTypes = java.util.Collections.unmodifiableList(__idList);
        __entityPropertyTypes = java.util.Collections.unmodifiableList(__list);
        __entityPropertyTypeMap = java.util.Collections.unmodifiableMap(__map);
    }

    @Override
    public org.seasar.doma.jdbc.entity.NamingType getNamingType() {
        return __namingType;
    }

    @Override
    public boolean isImmutable() {
        return __immutable;
    }

    @Override
    public String getName() {
        return __name;
    }

    @Override
    public String getCatalogName() {
        return __catalogName;
    }

    @Override
    public String getSchemaName() {
        return __schemaName;
    }

    @Override
    public String getTableName() {
        return getTableName(org.seasar.doma.jdbc.Naming.DEFAULT::apply);
    }

    @Override
    public String getTableName(java.util.function.BiFunction<org.seasar.doma.jdbc.entity.NamingType, String, String> namingFunction) {
        if (__tableName.isEmpty()) {
            return namingFunction.apply(__namingType, __name);
        }
        return __tableName;
    }

    @Override
    public boolean isQuoteRequired() {
        return __isQuoteRequired;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void preInsert(entity.Pattern entity, org.seasar.doma.jdbc.entity.PreInsertContext<entity.Pattern> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.preInsert(entity, context);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void preUpdate(entity.Pattern entity, org.seasar.doma.jdbc.entity.PreUpdateContext<entity.Pattern> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.preUpdate(entity, context);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void preDelete(entity.Pattern entity, org.seasar.doma.jdbc.entity.PreDeleteContext<entity.Pattern> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.preDelete(entity, context);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void postInsert(entity.Pattern entity, org.seasar.doma.jdbc.entity.PostInsertContext<entity.Pattern> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.postInsert(entity, context);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void postUpdate(entity.Pattern entity, org.seasar.doma.jdbc.entity.PostUpdateContext<entity.Pattern> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.postUpdate(entity, context);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void postDelete(entity.Pattern entity, org.seasar.doma.jdbc.entity.PostDeleteContext<entity.Pattern> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.postDelete(entity, context);
    }

    @Override
    public java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<entity.Pattern, ?>> getEntityPropertyTypes() {
        return __entityPropertyTypes;
    }

    @Override
    public org.seasar.doma.jdbc.entity.EntityPropertyType<entity.Pattern, ?> getEntityPropertyType(String __name) {
        return __entityPropertyTypeMap.get(__name);
    }

    @Override
    public java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<entity.Pattern, ?>> getIdPropertyTypes() {
        return __idPropertyTypes;
    }

    @Override
    public org.seasar.doma.jdbc.entity.GeneratedIdPropertyType<java.lang.Object, entity.Pattern, ?, ?> getGeneratedIdPropertyType() {
        return null;
    }

    @Override
    public org.seasar.doma.jdbc.entity.VersionPropertyType<java.lang.Object, entity.Pattern, ?, ?> getVersionPropertyType() {
        return null;
    }

    @Override
    public entity.Pattern newEntity(java.util.Map<String, org.seasar.doma.jdbc.entity.Property<entity.Pattern, ?>> __args) {
        entity.Pattern entity = new entity.Pattern();
        __args.values().forEach(v -> v.save(entity));
        return entity;
    }

    @Override
    public Class<entity.Pattern> getEntityClass() {
        return entity.Pattern.class;
    }

    @Override
    public entity.Pattern getOriginalStates(entity.Pattern __entity) {
        return null;
    }

    @Override
    public void saveCurrentStates(entity.Pattern __entity) {
    }

    /**
     * @return the singleton
     */
    public static _Pattern getSingletonInternal() {
        return __singleton;
    }

    /**
     * @return the new instance
     */
    public static _Pattern newInstance() {
        return new _Pattern();
    }

    private static class ListenerHolder {
        private static org.seasar.doma.jdbc.entity.NullEntityListener<entity.Pattern> listener = new org.seasar.doma.jdbc.entity.NullEntityListener<>();
    }

}
