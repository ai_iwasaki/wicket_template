package wicket.template;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import javax.inject.Inject;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.seasar.doma.jdbc.UniqueConstraintException;
import org.seasar.doma.jdbc.tx.TransactionManager;

import entity.Pattern;
import entity.User;

public class LoginPage extends WebPage {

	@Inject
	private UserDao dao;
	private User user;
	
	PageParameters parameters;

	public LoginPage(final PageParameters parameters) {
		super(parameters);
	}
	
	public LoginPage() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();
		user = new User();

		Form<Void> form = new Form<>("form");
		add(form);

		TextField<Integer> text1 = new TextField<>("userName",
				new PropertyModel<Integer>(user, "userName"));
		form.add(text1);

		TextField<Double> text2 = new TextField<>("password",
				new PropertyModel<Double>(user, "password"));
		form.add(text2);

		FeedbackPanel feedbackPanel = new FeedbackPanel("feedback") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void onConfigure() {
				// TODO Auto-generated method stub
				super.onConfigure();
				setOutputMarkupId(true);
			}
		};

		Button b1 = new AjaxButton("login") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			//
			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				super.onSubmit(target, form);
				try {
					 TransactionManager tm1 =
					 DBConfig.singleton().getTransactionManager();
					 tm1.required(() -> {
						 User userPass  = dao.select(user.userName).get();
						 
						 if(userPass.password.equals(user.password)){
							 SearchPage homePage = new SearchPage(user);
							 setResponsePage(homePage);
						 }
						 else{
							 info("ユーザネームまたはパスワードが違います");
							 target.add(feedbackPanel);
						 }
					 });
					 
					 
				} catch (NoSuchElementException e) {
					info("ID・パスワードを入力してください");
					target.add(feedbackPanel);
				}
				

			}
		};
		form.add(b1);
		form.add(feedbackPanel);
		
		Link<String> link = new Link<String>("createId") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				CreateIdPage createIdPage = new CreateIdPage();
				setResponsePage(createIdPage);
			}
		};

		form.add(link);

	}

}
