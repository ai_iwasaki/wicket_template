package wicket.template;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

import entity.Pattern;

public class DetailPanel extends Panel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DetailPanel(String id) {
		super(id);
	}
	
	
	@Override
	protected void onInitialize(){
		super.onInitialize();
	}
	
	
	@Override
	protected void onConfigure() {
		super.onConfigure();
		setOutputMarkupPlaceholderTag(true);
	}

}
