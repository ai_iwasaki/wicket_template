package wicket.template;

import java.util.List;
import java.util.Optional;

import org.seasar.doma.Dao;
import org.seasar.doma.Insert;
import org.seasar.doma.Select;
import org.seasar.doma.Update;

import entity.Pattern;


@Dao(config = DBConfig.class)
public interface PatternDao {

	@Select
	public Optional<Pattern> select(String patternName); //0~n件のときはList。Optionalは0~1件。Pattern:1件。
	
	@Select
	public List<String> selectItem(String userName);
	
	@Insert
	public int insert(Pattern pattern);
	
	@Update
	public int update(Pattern pattern);
}
